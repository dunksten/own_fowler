package fowler;

class Movie {
    private final String title;
    private final PriceCode priceCode;

    Movie(String newtitle, PriceCode newPriceCode) {
        title = newtitle;
        priceCode = newPriceCode;
    }

    PriceCode getPriceCode() {
        return this.priceCode;
    }

    String getTitle() {
        return this.title;
    }
}