package fowler;

import org.junit.Assert;
import org.junit.Test;

public class RentalTest {

    @Test
    public void initialize() {
        Movie movie = new Movie("title", PriceCode.REGULAR);
        Rental rental = new Rental(movie, 30);

        Assert.assertEquals(movie, rental.getMovie());
        Assert.assertEquals(30, rental.getDaysRented());
    }

}
