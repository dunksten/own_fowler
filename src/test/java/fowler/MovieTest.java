package fowler;

import org.junit.Assert;
import org.junit.Test;

public class MovieTest {

    @Test
    public void initializeVariables() {
        Movie movie = new Movie("title", PriceCode.REGULAR);
        Assert.assertEquals(PriceCode.REGULAR, movie.getPriceCode());
        Assert.assertEquals("title", movie.getTitle());
    }

    @Test
    public void testGetter() {
        Movie movie = new Movie("title", PriceCode.NEW_RELEASE);
        Assert.assertEquals(PriceCode.NEW_RELEASE, movie.getPriceCode());
    }
}
